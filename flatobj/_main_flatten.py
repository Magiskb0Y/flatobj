def flatten_dict(origin, flatten, origin_key=None, flatten_array=False, sep="_"):
    for (key, value) in origin.items():
        new_key = key
        if origin_key:
            new_key = f"{origin_key}{sep}{key}"
        if isinstance(value, dict):
            flatten = flatten_dict(value, flatten, new_key, flatten_array, sep)
        elif flatten_array and isinstance(value, list):
            flatten = flatten_list(value, flatten, new_key, True, sep)
        else:
            flatten[new_key] = value
    return flatten


def flatten_list(origin, flatten, origin_key=None, flatten_array=False, sep="_"):
    for (key, value) in enumerate(origin):
        new_key = key
        if origin_key:
            new_key = f"{origin_key}{sep}{key}"
        if isinstance(value, dict):
            flatten = flatten_dict(value, flatten, new_key, flatten_array, sep)
        elif flatten_array and isinstance(value, list):
            flatten = flatten_list(value, flatten, new_key, True, sep)
        else:
            flatten[new_key] = value
    return flatten


if __name__ == "__main__":
    origin = {
        "name": {
            "last_name": "Thanh",
            "first_name": "Nguyen Khac"
        },
        "age": 20,
        "address": {
            "city": "Hanoi",
            "country": {
                "name": "Vietnam",
                "code": "vi"
            }
        },
        "dict": {
            "values": [1, 2, 3]
        }
    }

    flatten = {}
    flatten = flatten_dict(origin, flatten, None, True, ".")
    print(flatten)