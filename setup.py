import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="flatobj",
    version="0.0.1",
    author="Nguyen Khac Thanh",
    author_email="nguyenkhacthanh244@gmail.com",
    description="A small package to help flatten dict",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/thanhngk/flatobj",
    packages=setuptools.find_packages(),
    license="MIT",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)